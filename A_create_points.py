import pandas as pd
import geopandas as gp
from shapely import wkt
from shapely.geometry import MultiPoint
import matplotlib.pyplot as plt

# NOMBRE CIUDAD
CIUDAD = "tijuana"
# Road network file from Marco Geoestadístico Nacional (EEe.gpkg)
archivo_calles = "calles_tj.gpkg"

nombre_puntos = "puntos_segmentos_%s.gpkg"%CIUDAD
distancia = 40 # distance between points
#proyeccion = 32615    # EPSG code

layer = gp.read_file(archivo_calles)


#layer = layer.to_crs(epsg = proyeccion)     

layer["longitud"] = round(layer.geometry.length)
layer = layer.reset_index()
layer["longitud_int"] = [int(layer.longitud[i]) for i in range(0, layer.shape[0])]

df = pd.DataFrame()
df["numeros"] = range(0,layer.shape[0])

# list comprehension doble, pero funciona
df["geometry"] = [MultiPoint([layer.geometry[j].interpolate(i) for i in range(0, layer.longitud_int[j], distancia)]) for j in range(0, df.shape[0])]

df = gp.GeoDataFrame(df, crs = layer.crs)
df = df.explode()
#df = df.to_crs(epsg = 4326)

# eliminar duplicados
df["geometry"] = df["geometry"].apply(lambda geom: geom.wkt)
df = df.drop_duplicates(["geometry"])
df["geometry"] = df["geometry"].apply(lambda geom: wkt.loads(geom))

df = gp.GeoDataFrame(df, crs = layer.crs) # no sé por qué ahora no queda como geometría el paso anterior
df.plot()
plt.show()

# DESPUÉS DE ESTO👀️ SE USA DBSCAN PARA CREAR CLUSTERS A X DISTANCIA (CLUSTER SIZE: 2, MAX DISTANCE: 20)
# LUEGO SE USA 👁️‍🗨️️"RANDOM SELECTION WITHIN SUBSETS" Y SE BORRAN MANUALMENTE DE LA CAPA
df.to_file(nombre_puntos, driver = "GPKG")


