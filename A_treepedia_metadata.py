# -*- coding: utf-8 -*-

import requests
import pandas as pd
import time
import argparse
import random

def prCyan(rint, skk):
  print("\033[{}m {}\033[00m" .format(rint, skk))


# PARSER  -------------------------------- --------------------
parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
    description='''Algoritmo Treepedia_mx, parte 2:
Consulta/descarga de metadatos de panoramas.
              _         _
  __   ___.--'_`.     .'_`--.___   __
 ( _`.'. -   'o` )   ( 'o`   - .`.'_ )
 _\.'_'      _.-'     `-._      `_`./_
( \`. )    //\`         '/\\    ( .'/ )
 \_`-'`---'\\__,       ,__//`---'`-'_/
  \`        `-\         /-'        '/
   `                               '   VK''',
    epilog="""Elio Lagunes Díaz, %(prog)s, https://gitlab.com/datamarindo.""", add_help = False)

parser.add_argument('archivo_entrada', type = str, help = "Nombre archivo de puntos segmentos (CSV)")
parser.add_argument('archivo_salida', type = str, help = "Nombre del archivo de salida (CSV)")
parser.add_argument('api_key', type = str, help = "Nombre del archivo con la API-KEY")
parser.add_argument('empezar_en',  type = str,  help = 'Empezar por este número')
parser.add_argument('terminar_en', type = str, help = 'Terminar en este número')
parser.add_argument('-h', '--help', action = 'help', default=argparse.SUPPRESS,
                    help = 'Mostrar este mensaje y salir.')
parser._positionals.title = 'Argumentos posicionales'
parser._optionals.title = 'Argumentos opcionales'

args = parser.parse_args()

# INPUTS ------------------------------------------------------
puntos = args.archivo_entrada
nombre_archivo = args.archivo_salida 
kf = args.api_key
inicio = int(args.empezar_en)
fin = int(args.terminar_en)


# LECTURAS ----------------------------------------------------
layer = pd.read_csv(puntos)       
kfo = open(kf, "r")
key = kfo.read().splitlines()[0]
kfo.close()


# DESCARGA ----------------------------------------------------
for i in range(inicio, fin):    
	url = "https://maps.googleapis.com/maps/api/streetview/metadata?size=400x400&location=%s,%s&fov=80&heading=120&pitch=0&sensor=false&key=%s"%(layer.Y[i], layer.X[i], key)
	resp = requests.get(url)
	time.sleep(random.randrange(1,2))
	data = resp.json()    
	puntos = pd.json_normalize(data)
	puntos["numeroconsulta"] = i
	puntos.to_csv(nombre_archivo, mode = "a", header = False, index = False, encoding='utf-8')     
	prCyan(random.choice([*range(90,97,1)]), "metadato número %s; status: %s"%(i, pd.json_normalize(data)["status"][0]))
	if pd.json_normalize(data)["status"][0] == "OVER_QUERY_LIMIT":
		break
		print("Metadato num: %s, provocó una salida OVER_QUERY_LIMIT"%(local_filename, i))

		

