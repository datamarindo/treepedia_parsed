# -*- coding: utf-8 -*-

import pandas as pd
import geopandas as gp
import glob
import argparse


# PARSER  -------------------------------- --------------------
parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
    description='''Algoritmo Treepedia_mx, parte 4: juntar archivos GVI
              _         _
  __   ___.--'_`.     .'_`--.___   __
 ( _`.'. -   'o` )   ( 'o`   - .`.'_ )
 _\.'_'      _.-'     `-._      `_`./_
( \`. )    //\`         '/\\    ( .'/ )
 \_`-'`---'\\__,       ,__//`---'`-'_/
  \`        `-\         /-'        '/
   `                               '   VK''',
    epilog="""Elio Lagunes Díaz, %(prog)s, https://gitlab.com/datamarindo.""", add_help = False)

parser.add_argument('patron', type = str, help = "Patrón de archivos GVI")
parser.add_argument('archivo_panoid',  type = str,  help = 'Archivo panoid*.csv, entrecomillado')
parser.add_argument('archivo_salida', type = str, help = "Nombre del archivo de salida + .js, .gpkg o .shp")
parser.add_argument('formato', type = str, help = "formato de salida (GPKG, js o 'ESRI Shapefile')")
parser.add_argument('-h', '--help', action = 'help', default=argparse.SUPPRESS,
                    help = 'Mostrar este mensaje y salir.')
parser._positionals.title = 'Argumentos posicionales'
parser._optionals.title = 'Argumentos opcionales'

args = parser.parse_args()

# INPUTS ------------------------------------------------------

archivos_gvi = glob.glob(args.patron)    
archivo_metadatos = args.archivo_panoid                    
archivo_salida = args.archivo_salida
formato = args.formato

# -----
results_df = pd.DataFrame()
for i in archivos_gvi:
    temp = pd.read_csv(i)
    results_df = results_df.append(temp)    

results_df = results_df.reset_index()
results_df["pano_id"] = [i[:22] for i in results_df.pano_id]
val_mean = results_df.groupby("pano_id").mean()

pano_points = pd.read_csv(archivo_metadatos,  names = ['copyright', 'date', 'pano_id', 'status', 'lat', 'lon'])
pano_points = pano_points.query('copyright == "© Google"').reset_index()

gdf = gp.GeoDataFrame(
    pano_points, geometry = gp.points_from_xy(pano_points["lon"], pano_points["lat"]), crs = "EPSG:4326")
gdf = gdf.join(val_mean, on = "pano_id", how = "left", lsuffix = "pp", rsuffix = "vm")
gdf = gdf.filter(["GVI", "geometry"]).dropna(subset = ["GVI"])
gdf["GVI"] = round(gdf["GVI"], 1)


if formato == "js":
    gdf = "var gvi = %s" % gdf.to_json(drop_id = True)
    with open("gvi.js", "w") as outfile:
        outfile.write(gdf)
else:
    gdf.to_file(archivo_salida, driver = formato)
