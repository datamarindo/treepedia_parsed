# -*- coding: utf-8 -*-

import time
from PIL import Image
import os
import numpy as np
import requests
import pandas as pd
import argparse
import random

# revisar "ANSI escape codes para los colores en terminal"
def prCyan(rint, skk):
  print("\033[{}m {}\033[00m" .format(rint, skk))

parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
    description='''Treepedia_mx, parte 3: Descarga de panoramas.
.. . . . . . . . . . . . . . . . . . . . . . . . . . . . .
.. . . . . . . .#######. . . . . . . . . . . . . . . . . .
.. . . . . . .#. .#### . . . ####. . .###############. . .
.. . ########. ##. ##. . . ######################### . . .
.. . . ##########. . . . ######################. . . . . .
.. . . .######## . . . .   ################### . . . . . .
.. . . . ### .   . . . .#####. ##############. # . . . . .
.. . . . . ##### . . . .#######. ##########. . . . . . . .
.. . . . . .###### . . . .#### . . . . .## . . . . . . . .
.. . . . . . ##### . . . .#### # . . . . . ##### . . . . .
.. . . . . . ### . . . . . ##. . . . . . . . ### .#. . . .
.. . . . . . ##. . . . . . . . . . . . . . . . . . . . . .
.. . . . . . . . . . . Brice Wellington from Winston Smith
''',
    epilog="""Elio Lagunes Díaz, %(prog)s, https://gitlab.com/datamarindo.""", add_help = False)

parser.add_argument('archivo_entrada', type = str, help = "Nombre del archivo de metadatos")
parser.add_argument('directorio_salida', type = str, help = "Nombre del directorio de salida")
parser.add_argument('api_key', type = str, help = "Nombre del archivo con la API-KEY")
parser.add_argument('empezar_en',  type = str,  help = 'Empezar por este número')
parser.add_argument('terminar_en', type = str, help = 'Terminar en este número')
parser.add_argument('-h', '--help', action = 'help', default=argparse.SUPPRESS,
                    help = 'Mostrar este mensaje y salir.')
parser._positionals.title = 'Argumentos posicionales'
parser._optionals.title = 'Argumentos opcionales'

args = parser.parse_args()

# inputs
archivo_metadatos = args.archivo_entrada
kf = args.api_key
kfo = open(kf, "r")
key = kfo.read().splitlines()[0]
kfo.close()
directorio = args.directorio_salida
inicio = int(args.empezar_en)
fin = int(args.terminar_en)                  

#--------

headingArr = 360/4*np.array([1,3])              
numGSVImg = len(headingArr)*1.0                  
pitch = 0

data = pd.read_csv(archivo_metadatos, names = ["copyright", "fecha", "lat", "lon", "pano", "status"])
data = data.drop_duplicates()
data = data.reset_index()

for i in range(inicio, fin): 
	pano = data.pano[i]
	for heading in headingArr:
		URL = "http://maps.googleapis.com/maps/api/streetview?size=600x300&pano=%s&fov=60&heading=%d&pitch=%d&sensor=false&key=%s"%(pano,heading,pitch, key)
		local_filename = "%s/%s_%s.jpg"%(directorio, pano, heading)
		r = requests.get(URL)        
		f = open(local_filename, 'wb')
		f.write(r.content)
		f.close()
		time.sleep(random.randrange(1,3))
		prCyan(random.choice([*range(90,97,1), *range(100,106,1)]), "imagen guardada como %s, num: %s"%(local_filename, i))
		if os.path.getsize(local_filename) <= 6000:
			print("La imagen %s, num: %s salio con error"%(local_filename, i))
			break
	else:
		continue
	break


