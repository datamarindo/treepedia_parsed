from tkinter import *

import turtle
from turtle import Screen
import math

screen = Screen()
screen.setup(600, 480)  # what's visible
screen.screensize(1200, 960)  # total backing store


i = 100
turtle.clearscreen()
turtle.bgcolor("pink")
turtle.shape("turtle")
turtle.turtlesize(.5,.5,.5)
turtle.speed(0)

turtle.penup()
turtle.goto(-340,200)
turtle.pendown()
turtle.setheading(0)

5 % 2

for y in range(6):
  for x in range(8):
    for j in range(3):
      turtle.fd(i)
      turtle.lt(120+0)
  
    turtle.lt(30+0)
    turtle.fd(i*math.sqrt(3)/3)
    turtle.lt(60+0)
    turtle.fd(i*math.sqrt(3)/3)
    turtle.rt(150-0)
    turtle.fd(i)
    turtle.rt(150-0)
    turtle.fd(i*math.sqrt(3)/3)
    turtle.rt(180-0)
    turtle.fd(i*math.sqrt(3)/3)
    turtle.setheading(0-0)
  
  
  turtle.lt(180)
  for x in range(9) if y % 2 > 0 else range(8):
    for j in range(3):
      turtle.fd(i)
      turtle.lt(120+0)
  
    turtle.lt(30+0)
    turtle.fd(i*math.sqrt(3)/3)
    turtle.lt(60+0)
    turtle.fd(i*math.sqrt(3)/3)
    turtle.rt(150-0)
    turtle.fd(i-0)
    turtle.rt(150+0)
    turtle.fd(i*math.sqrt(3)/3)
    turtle.rt(180-0)
    turtle.fd(i*math.sqrt(3)/3)
    turtle.setheading(180-0)
  
  turtle.lt(120+0)
  turtle.fd(i)
  turtle.setheading(0-0)


canvas = turtle.getcanvas()
canvas.postscript(file="test.eps", x=-600, y=-480, width=1200, height=960)

ts = turtle.getscreen()
dir(ts.getcanvas)
ts.getcanvas().postscript(file = "nezuko.eps")
