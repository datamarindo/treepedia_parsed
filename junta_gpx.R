library(sf)
library(dplyr)
library(lubridate)
library(stringdist)

ea = list.files("/home/elio/rest_chinampas/macuil/", pattern = "\\.gpx", full.names = T)

chinampas = lapply(ea,  function(x) read_sf(x, layer = "waypoints"))

chinampas = bind_rows(chinampas)

chinampas = chinampas %>% filter(time > as.Date("2021-07-10")) %>%
  as_tibble() %>% distinct(geometry, desc, sym, time, cmt) %>% st_as_sf()


chinampas %>% filter(!if_all(c(cmt, desc), is.na)) %>% 
  mutate(fecha = strftime(time, "%Y-%m-%d") ) %>%
  write_sf("chinampas_macuil.gpkg")


c(chinampas$cmt[chinampas$cmt != "las palomas"], chinampas$desc)[!is.na(c(chinampas$cmt[chinampas$cmt != "las palomas"], chinampas$desc))] %>%
qgrams(chinampas$cmt[chinampas$cmt != "las palomas"], chinampas$desc) %>% colSums()


chinampas = chinampas %>% 
  filter(!if_all(c(cmt, desc), is.na))

qgrams(c(chinampas$cmt[!is.na(chinampas$cmt)], 
           chinampas$desc[!is.na(chinampas$desc)]) ) 

sort(table(chinampas$desc[!is.na(chinampas$desc)]))
table(chinampas$cmt[!is.na(chinampas$cmt)])
  

### qgram PASADAS ------------------
ea = read_sf("/home/elio/rest_chinampas/chinampas2.gpkg")

ea = ea %>% mutate(desc = tolower(desc))
ea %>% mutate(time = strftime(time, format = "%Y-%m-%d") %>% as.Date(.)) %>%
  group_by(sitio) %>%
  summarise(min = min(time, na.rm = T), max = max(time, na.rm = T))

ea %>% count(sitio)
qgrams("Flota" = ea$desc[ea$sitio == "La Flota"],  
       "Pájaro" = ea$desc[ea$sitio == "El Pájaro"], 
       "Culebrilla" = ea$desc[ea$sitio == "La Culebrilla"]) %>%
  as.data.frame() %>%
  write.csv("plantas_por_predio_restauracion_backcountry.csv")

       