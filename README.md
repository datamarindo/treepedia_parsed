# Treepedia parser

Es la versión del algoritmo Treepedia del MIT SenseableCities para ser usada desde línea de comando, adaptada para trabajar con Python 2 o 3; 

Archivos:

1. A_treepedia_metadata_py2: Descarga los metadatos de los panoramas a partir de un CSV con las coordenadas de puntos distanciados 40 m sobre la red vial de una ciudad; su salida es un CSV con el id de los panoramas y sus coordenadas.
1. B_pano_downloader_parser: Descarga los panoramas desde un listado CSV con los id de panoramas, su salida es la descarga de todos los panoramas del listado a un directorio.
1. C_green_view_calculate: Calcula el índice de vista verde de cada panorama, su output es un CSV con el GVI de cada punto del directorio seleccionado. Se hace por separado para cada directorio porque es un proceso computacionalmente intensivo y demora más de una hora por cada 10,000 panoramas de un directorio en una computadora potente.
1. D_gvi_collector: junta todos los archivos de GVI*.csv generados por cada directorio en el paso anterior y les junta las coordenadas desde el archivo producido en el primer paso. Su salida es un archivo vectorial.

# Para obtener la API Key de Google

Es necesario darse de alta en cloud.google, es necesario contar con __tarjeta de débito o crédito__
1. Crear un proyecto
1. Activar la API de Maps Static > Streetview (en APIs "+ Habilitar API y Servicios")
1. Crear una credencial ("APIs > Credenciales  > + Crear Credenciales")

# Para ejecutar los archivos:

## Linux y Mac:
Es necesario tener instalado python y los módulos: pandas, geopandas, PIL, pymeanshift, numpy, multiprocessing y requests.
con `pip install {nombre del módulo}`

Conceder permisos de ejecución:

`sudo chmod +x nombre_archivo.py` 

Leer los argumentos posicionales requeridos usando la ayuda (`-h`) 

`python A_treepedia_metadata_py2.py -h`

## Windows
Lo más sencillo es instalar Miniconda, desde https://docs.conda.io/en/latest/miniconda.html;
ahí crear un ambiente, por ejemplo:
`conda create -n treepedia` 

y dentro de este instalar con:
`conda install pandas geopandas Pillow numpy requests`

ya instaladas las dependencias:
`python B_pano_downloader_py2.py -h` imprime la ayuda para la función

En windows no está disponible el algoritmo PyMeanShift
