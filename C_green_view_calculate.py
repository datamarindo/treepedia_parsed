# -*- coding: utf-8 -*-

import pymeanshift as pms
import numpy as np
import matplotlib.pyplot as plt
import os
import pandas as pd
import multiprocessing as mp
import geopandas as gp
from PIL import Image
import glob
import argparse


# PARSER  -------------------------------- --------------------
parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
    description='''Algoritmo Treepedia_mx, parte 4: calcular GVI de archivos JPG
    _
  .'_`--.___   __
 ( 'o`   - .`.'_ )
  `-._      `_`./_
    '/\\    ( .'/ )
   ,__//`---'`-'_/
     /-'        '/ VK''',
    epilog="""Elio Lagunes Díaz, %(prog)s, https://gitlab.com/datamarindo.""", add_help = False)

parser.add_argument('directorio', type = str, help = "Directorio con panoramas (JPG)")
parser.add_argument('archivo_salida', type = str, help = "Nombre del archivo de salida (CSV)")
parser.add_argument('-h', '--help', action = 'help', default=argparse.SUPPRESS,
                    help = 'Mostrar este mensaje y salir.')
parser._positionals.title = 'Argumentos posicionales'
parser._optionals.title = 'Argumentos opcionales'

args = parser.parse_args()

# INPUTS ------------------------------------------------------

directorio = args.directorio
nombre_csv_salida = args.archivo_salida

# ------

def graythresh(array,level):
    '''array: is the numpy array waiting for processing
    return thresh: is the result got by OTSU algorithm
    if the threshold is less than level, then set the level as the threshold
    by Xiaojiang Li
    '''
    
    import numpy as np 
    maxVal = np.max(array)
    minVal = np.min(array)    
#   if the inputImage is a float of double dataset then we transform the data 
#   in to byte and range from [0 255]
    if maxVal <= 1:
        array = array*255
        # print "New max value is %s" %(np.max(array))
    elif maxVal >= 256:
        array = np.int((array - minVal)/(maxVal - minVal))
        # print "New min value is %s" %(np.min(array))    
    # turn the negative to natural number
    negIdx = np.where(array < 0)
    array[negIdx] = 0    
    # calculate the hist of 'array'
    dims = np.shape(array)
    hist = np.histogram(array,range(257))
    P_hist = hist[0]*1.0/np.sum(hist[0])    
    omega = P_hist.cumsum()    
    temp = np.arange(256)
    mu = P_hist*(temp+1)
    mu = mu.cumsum()    
    n = len(mu)
    mu_t = mu[n-1]    
    sigma_b_squared = (mu_t*omega - mu)**2/(omega*(1-omega))    
    # try to found if all sigma_b squrered are NaN or Infinity
    indInf = np.where(sigma_b_squared == np.inf)    
    CIN = 0
    if len(indInf[0])>0:
        CIN = len(indInf[0])    
    maxval = np.max(sigma_b_squared)    
    IsAllInf = CIN == 256
    if IsAllInf !=1:
        index = np.where(sigma_b_squared==maxval)
        idx = np.mean(index)
        threshold = (idx - 1)/255.0
    else:
        threshold = level    
    if np.isnan(threshold):
        threshold = level    
    return threshold


def VegetationClassification(Img):
    '''
    This function is used to classify the green vegetation from GSV image,
    This is based on object based and otsu automatically thresholding method
    The season of GSV images were also considered in this function
        Img: the numpy array image, eg. Img = np.array(Image.open(StringIO(response.content)))
        return the percentage of the green vegetation pixels in the GSV image
    
    By Xiaojiang Li
    '''  
    import pymeanshift as pms
    import numpy as np
    # use the meanshift segmentation algorithm to segment the original GSV image
    (segmented_image, labels_image, number_regions) = pms.segment(Img,spatial_radius=6,
                                                     range_radius=7, min_density=40)
    
    I = segmented_image/255.0
    red = I[:,:,0]
    green = I[:,:,1]
    blue = I[:,:,2]    
    # calculate the difference between green band with other two bands
    green_red_Diff = green - red
    green_blue_Diff = green - blue    
    ExG = green_red_Diff + green_blue_Diff
    diffImg = green_red_Diff*green_blue_Diff    
    redThreImgU = red < 0.6
    greenThreImgU = green < 0.9
    blueThreImgU = blue < 0.6    
    shadowRedU = red < 0.3
    shadowGreenU = green < 0.3
    shadowBlueU = blue < 0.3
    del red, blue, green, I    
    greenImg1 = redThreImgU * blueThreImgU*greenThreImgU
    greenImgShadow1 = shadowRedU*shadowGreenU*shadowBlueU
    del redThreImgU, greenThreImgU, blueThreImgU
    del shadowRedU, shadowGreenU, shadowBlueU    
    greenImg3 = diffImg > 0.0
    greenImg4 = green_red_Diff > 0
    threshold = graythresh(ExG, 0.1)    
    if threshold > 0.1:
        threshold = 0.1
    elif threshold < 0.05:
        threshold = 0.05    
    greenImg2 = ExG > threshold
    greenImgShadow2 = ExG > 0.05
    greenImg = greenImg1*greenImg2 + greenImgShadow2*greenImgShadow1
    del ExG,green_blue_Diff,green_red_Diff
    del greenImgShadow1,greenImgShadow2    
    # calculate the percentage of the green vegetation
    greenPxlNum = len(np.where(greenImg != 0)[0])
    greenPercent = greenPxlNum/(400.0*400)*100
    del greenImg1,greenImg2
    del greenImg3,greenImg4    
    return greenPercent

panos = os.listdir(directorio)

def pano_gvi(pano):
    try:
        im = np.array(Image.open("%s/%s"%(directorio, pano)))
        percent = VegetationClassification(im)
        dp = {'pano_id': [pano], 'GVI': [percent]}
        dfp = pd.DataFrame(data = dp)
        return dfp
    except:
        pass

pool = mp.Pool(processes = 8)

results = pool.map(pano_gvi, panos)
pool.close()
pool.join()
results_df = pd.concat(results)
results_df.to_csv(nombre_csv_salida, index = False)




